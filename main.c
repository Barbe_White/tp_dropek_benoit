/*
 * DROPEK Benoît
 */

#include <stdio.h>
#include "trees.h"



int main (int argc, char *argv[])
{
    node *Arbre ;

    mk_empty_tree(&Arbre);

    FILE* fp ;
	fp = fopen(argv,"r"); //ouverture en lecture

	load_tree(fp, &Arbre);
	print_tree(&Arbre);
	free_tree(&Arbre);


	return EXIT_SUCCESS;
}
