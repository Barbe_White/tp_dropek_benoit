#ifndef TREES_C_INCLUDED
#define TREES_C_INCLUDED



#endif // TREES_C_INCLUDED

/*
 * DROPEK Benoît
 */

#include "trees.h"

// construction of a tree by pointer
void cons_tree(struct node ** ptr_tree, int val, struct node *left, struct node *right)
{
    node *tmpNode;
    node *tmpTree = *ptr_tree;

    node *elem = malloc(sizeof(node));

    elem->val = val;
    elem->left = left;
    elem->right = right;

    if(tmpTree)
    do
    {
        tmpNode = tmpTree;
        if(val > tmpTree->val )
        {
            tmpTree = tmpTree->right;
            if(!tmpTree) tmpNode->right = elem;
        }
        else
        {
            tmpTree = tmpTree->left;
            if(!tmpTree) tmpNode->left = elem;
        }
    }
    while(tmpTree);
    else  *ptr_tree = elem;


}


// initialize un empty tree
void mk_empty_tree(struct node ** ptr_tree)
{
	*ptr_tree = NULL;
}

// is tree empty?
bool is_empty(struct node *tree)
{
	return tree==NULL;
}

// is tree a leaf?
bool is_leaf(struct node *tree)
{
	return(!is_empty(tree) && is_empty(tree->left) && is_empty(tree->right));
}

// add x in a bst wtr its value.
void add(struct node **ptr_tree, int x)
{
	node *tmpNode;
    node *tmpTree = *ptr_tree;

    node *elem = malloc(sizeof(node));
    elem->val = x;
    elem->left = NULL;
    elem->right = NULL;

    if(tmpTree)
    do
    {
        tmpNode = tmpTree;
        if(x > tmpTree->val )
        {
            tmpTree = tmpTree->right;
            if(!tmpTree) tmpNode->right = elem;
        }
        else
        {
            tmpTree = tmpTree->left;
            if(!tmpTree) tmpNode->left = elem;
        }
    }
    while(tmpTree);
    else  *ptr_tree = elem;
}

// print values of tree in ascendant order
void print_tree(struct node *tree)
{
	 if(!tree) return;

    if(tree->right) print_tree(tree->right);

    printf("Val = %d\n", tree->val);

    if(tree->left)  print_tree(tree->left);
}

// build a tree "add"ing values of the file fp
void load_tree(FILE *fp, struct node **ptr_tree)
{
    int a;

	if (fp!=NULL)
	{
		//fichier ouvert avec succes !
	        fscanf(fp,"%d",&a);
		while (!feof(fp))
		{
			//feof APRES la premiere lecture.


            add(ptr_tree, a);
	        fscanf(fp,"%d",&a);
		}
		fclose(fp);//fermeture
	}
}

// Free all memory used by the tree
void free_tree(struct node **ptr_tree)
{
	node *tmpTree = *ptr_tree;

    if(!ptr_tree) return;

    if(tmpTree->left)  free_tree(&tmpTree->left);

    if(tmpTree->right) free_tree(&tmpTree->right);

    free(tmpTree);

    *ptr_tree = NULL;
}

//fonction max
int maxi (int a, int b)
{
    if (a>b)
        return a;
    else
        return b;
}

//fonction hauteur max
int height_tree (struct node *A)
{
    if(A==NULL)
    return 0;

    if((A->left == NULL) && (A->right == NULL))
        return 1;
    else
        return 1 + maxi(height_tree(A->left),height_tree(A->right));
}

//fonction nombre mini d'un arbre
int get_min ( struct node *A)
{
	while (A->left != NULL)
		{
			A = A->left;
		}
	return A->val;

}

//fonction nombre maxi d'un arbre
int get_max ( struct node *A)
{
	while (A->right != NULL)
		{
			A = A->right;
		}
	return A->val;

}

//recherche dans un arbre
int tree_search(node *tree, unsigned int val)
{
    while(tree)
    {
        if(val == tree->val) return 1;

        if(val > tree->val ) tree = tree->right;
        else tree = tree->left;
    }
    return 0;
}
