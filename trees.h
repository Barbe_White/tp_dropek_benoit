#ifndef TREES_H_INCLUDED
#define TREES_H_INCLUDED



#endif // TREES_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct node {
	int val;
	struct node *left;
	struct node *right;
}node, *PtNode, *Tree;
/* These typedefs are optionnel, you may use them */


/* Constructs a new tree from a value for the root node, a given left tree and a given right tree */
void cons_tree(struct node **, int, struct node *, struct node *);

/* Make an empty tree */
void mk_empty_tree(struct node **);

/* Is the tree empty ? */
bool is_empty(struct node *);

/* Is the tree a leaf ? */
bool is_leaf(struct node *);

/* Adds the value (int) to the binary search tree,
 * it must be ordered.
 * Duplicate values are valid.
 */
void add(struct node **, int);

/* Prints the values of the tree in ascendant order */
void print_tree(struct node *);

/* Builds a tree adding values of the file */
void load_tree(FILE *, struct node **);

/* Frees all the tree's memory */
void free_tree(struct node **);

//fonction max
int maxi (int , int );

//fonction hauteur max
int height_tree (struct node *);

//fonction nombre mini d'un arbre
int  get_min ( struct node *);

//fonction nombre maxi d'un arbre
int get_max ( struct node *);

//recherche dans un arbre
int tree_search(node *, unsigned int );


